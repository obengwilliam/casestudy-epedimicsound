import {Request, Response, NextFunction} from 'express'
import {HttpException, ErrorStatusMap} from '../lib/errors'

const errorMap = (mapping: ErrorStatusMap)=>(err: Error,  _req: Request, _res: Response, next: NextFunction): void =>{
  const status = mapping[err.name] !==undefined ? mapping[err.name] :null 
  next(new HttpException(status, err.message))
}


export default errorMap
