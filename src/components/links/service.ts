import jwt from 'jsonwebtoken'
import moment from 'moment'
import sendEmail from '../../lib/mail'
import config from '../../config'
import {TokenDetails, TokenPayload, TokenPayLoadWithExpiryDate, SendEmailDetails} from './types'
import {TokenVerificationError} from './error'
import log from '../../lib/log'

const KEY = config('TOKEN_PRIVATE_KEY')
const EXPIRES_HOURS = 2
const APPRUVE_FROM_EMAIL = config('APPRUVE_FROM_EMAIL')
const LINK_BASE_URL = config('LINK_BASE_URL')

const formLinkUrl = (token: string): string => `${LINK_BASE_URL as string}/${token}`

export async function createToken (payload: TokenPayload): Promise<TokenDetails> {

  const expiresAt = moment().add('hours', EXPIRES_HOURS).toISOString()
  const options = {expiresIn: `${EXPIRES_HOURS} hours`}

  const token = jwt.sign({...payload, ...{expiresAt}}, KEY, options)
  return {token, expiresAt}
}


export async function sendEmailToCustomer (sendEmailDetails: SendEmailDetails): Promise<string> {
  const {token} = await createToken(sendEmailDetails)
  const linkUrl = formLinkUrl(token)

  const html = `<strong>${linkUrl}} </strong>`
  return await sendEmail({
    to: sendEmailDetails.email,
    subject: 'Requesting Access To Financial data',
    from: APPRUVE_FROM_EMAIL as string,
    html
  }).then(_ => sendEmailDetails.email)
}

export async function verifyToken (token: string): Promise<TokenPayLoadWithExpiryDate> {
  try {
    const decoded = jwt.verify(token, KEY) as TokenPayLoadWithExpiryDate

    return {
      merchantId: decoded.merchantId,
      expiresAt: decoded.expiresAt,
      accessLimitInMonths: decoded.accessLimitInMonths
    }
 
  } catch (err) {
    log.error(err)
    throw new TokenVerificationError()
  }
}
