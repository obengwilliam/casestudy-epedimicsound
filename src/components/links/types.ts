export interface TokenPayload {
  accessLimitInMonths: string
  merchantId: string
}


export interface TokenDetails {
  expiresAt: string
  token: string
}

export interface TokenPayLoadWithExpiryDate extends TokenPayload {
  expiresAt: string
}
export interface TokenResponse {
  expires_at: string
  token: string
}

export interface SendEmailDetails extends TokenPayload {
  email: string
}
export interface VerifiedTokenResponse {
  merchant_id: string
  access_limit_in_months: string
  expires_at: string
}
