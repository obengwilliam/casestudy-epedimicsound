import {Request, Response, NextFunction} from 'express'

import status from '../../lib/status'
import {createToken, verifyToken, sendEmailToCustomer as sendEmail} from './service'
import {TokenPayload, TokenResponse, TokenDetails, TokenPayLoadWithExpiryDate, VerifiedTokenResponse} from './types'

const responseTransformers = {
  createToken (tokenDetails: TokenDetails): TokenResponse {
    return {
      token: tokenDetails.token,
      expires_at: tokenDetails.expiresAt
    }
  },
  tokenDetails (decodedToken: TokenPayLoadWithExpiryDate): VerifiedTokenResponse {
    return {
      merchant_id: decodedToken.merchantId,
      expires_at: decodedToken.expiresAt,
      access_limit_in_months: decodedToken.accessLimitInMonths
    }
  }
}



export const createTokens = (req: Request, res: Response, next: NextFunction): void => {
  createToken(req.body as TokenPayload)
    .then(responseTransformers.createToken)
    .then((data: TokenResponse) => {
      res.status(status.OK).json({data})
    }).catch(next)
}



export const getTokens = (req: Request, res: Response, next: NextFunction): void => {
  verifyToken(req.params.token)
    .then(responseTransformers.tokenDetails)
    .then((data: VerifiedTokenResponse) => {
      res.status(status.OK).json({data})
    }).catch(next)
}



export const sendEmailToCustomer = (req: Request, res: Response, next: NextFunction): void => {
  sendEmail(req.body)
    .then(_ => {
      const data = {message: 'email sent'}
      res.status(status.OK).json({data})
    }).catch(next)
}


