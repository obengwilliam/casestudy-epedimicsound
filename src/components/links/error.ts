import {CustomError} from 'ts-custom-error'
import errorCodes from '../../lib/error-codes'

export class TokenVerificationError extends CustomError {
  code: string 
  constructor ( message?: string){
    super(message ?? 'token is not verifiable')
    this.code = errorCodes.ERR_TOKEN_VERIFICATION
  }
}

export class TokenError extends CustomError {
  code: string
  constructor ( message: string){
    super(message)
    this.code = errorCodes.ERR_TOKEN_VERIFICATION
  }
}


