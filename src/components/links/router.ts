import express from 'express'
import * as ctrl from './controller'
import {ErrorStatusMap} from '../../lib/errors'
import mapErrorToStatus from '../../middlewares/error-map-status'

const errorMap: ErrorStatusMap = {}

export default express.Router()
  .post('/tokens', ctrl.createTokens)
  .post('/send-email', ctrl.sendEmailToCustomer)
  .get('/tokens/:token', ctrl.getTokens)
  .use(mapErrorToStatus(errorMap))
