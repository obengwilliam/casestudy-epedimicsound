
import ZnBankGh from './zn-bank-gh'
import {Codes, Countries} from './types'

export default function fsFactory (code: Codes, country: Countries): ZnBankGh{
  if ( code === Codes.ZN && country === Countries.GH) {
    return new ZnBankGh()
  } else {
    throw Error('institution not available')
  } 
}
