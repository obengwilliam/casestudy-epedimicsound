import got from 'got'

const client = got.extend({
  retry :{
    limit: 5 
  },
  responseType: 'json'
})

export default client

