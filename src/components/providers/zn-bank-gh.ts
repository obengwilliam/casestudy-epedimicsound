import {  TypeOfAccount, SubTypeAccount} from '../accounts/types'
import client from './request'
import {prop, ifElse, equals, pipe,  always, applySpec} from 'ramda'
import {Codes, AuthenticateDetails,   AccountDetails, FinancialInstitution} from './types'

const code = Codes.ZN 

const transformations = {
  accounts: applySpec({
    customerName: prop('AccountName'),
    openingBalance: prop('OpeningBalance'),
    closingBalance: prop('AvailableBalance'),
    type: always(TypeOfAccount.BRK), 
    provider: always(code),
    currency: prop('IsoCurrency'),
    subtype:  pipe(prop('AccountType'), 
      ifElse(equals('SA'),
        always(SubTypeAccount.sav),
        always(SubTypeAccount.cur))),
    accountNumber: prop('AccountNumber'),

  })

}
export default class ZennithBank implements FinancialInstitution{
  code = code

  async authenticate (details: AuthenticateDetails): Promise<AccountDetails []>{
    const url  = 'https://apps.zenithbank.com.gh/zmobile/api/Mobile/AuthenticateReqPOST'
    const json = {
      clientAuth:{
        CallerID:'WmVuaXRoUGxjLTVDNEM1Q0E2M0EzQTI3Qjc3NkFDRjM0Qjc3OUM1',
        CallerReference:'8W7jDe315roWYhJiyvGxMj1QMvv813IM-DEFAD94D8CD6F',
        ClientName:'WmVuaXRoLWdoLTExY2pIU2F3VmUtNXEwZTJsek5yNQ\u003d\u003d',
        Password:'HE838`5P.\u0027s1]mqj+5B8{_cz?05nd!!R0xs9NK4OC166662ghdviI021bv7Kc,c'},
      customerAuth:{
        Accesscode:details.name,
        AuthenticationMode:1,
        DeviceType:'Android',
        PassCode: details.password, 
        TransactionAuthMode:1
      }
    }
    return   await client.post(url, {json}). 
      then(({body: data} )=>{
        const account = data as any
        return   account.LinkedAccounts.map(transformations.accounts)
        
      })
  }
}

