import {Request, Response, NextFunction} from 'express'
import {all} from './service'

import {Providers, ProvidersResponse} from './types'
import status from '../../lib/status'

const responseTransformers = {
  allProviders (providers: Providers): ProvidersResponse {
    return providers
  }
}

export function getAllProviders (_req: Request, res: Response, next: NextFunction): void {
  all()
    .then(responseTransformers.allProviders)
    .then((data) => {
      res.status(status.OK).json({data})
    })
    .catch(next)
}
