import {Banks, Countries, Providers, MobileMoney, Codes} from './types'

export const providerCodes = {
  [Banks.GT]: Codes.GT,
  [Banks.ZN]: Codes.ZN,
  [MobileMoney.MTN]: Codes.MM 
}

const products = [
  'account',
  'statements'
]

export const all = async (): Promise<Providers> => {

  return [
    {
      name: Banks.GT,
      code: providerCodes[Banks.GT],
      products,
      website: 'https://gtbank.com',
      icon: 'https://www.logosurfer.com/wp-content/uploads/2018/03/Guaranty20Trust20Bank20Logo.jpg',
      coverage: [
        {
          country: Countries.GH,
          business: true,
          personal: true
        }
      ]
    },

    {
      name: Banks.ZN,
      code: providerCodes[Banks.ZN],
      products,
      website: 'https://www.zenithbank.com/',
      icon: 'https://www.logosurfer.com/wp-content/uploads/2018/03/Guaranty20Trust20Bank20Logo.jpg',
      coverage: [
        {
          country: Countries.GH,
          business: true,
          personal: true
        }
      ]
    },
    {
      name: MobileMoney.MTN,
      code: providerCodes[MobileMoney.MTN],
      website: 'https://mtn.com',
      products,
      icon: 'https://www.mtn.com/wp-content/themes/mtn-theme/images/mtn-logo-nav.svg',
      coverage: [
        {
          country: Countries.GH,
          business: true,
          personal: true
        }
      ]
    }

  ]
}


