import {Account} from '../accounts/types'

export enum Codes {
  GT = 'bk_gt',
  ZN =  'bk_zn',
  MM = 'mm_mtn',
}
export enum Countries {
  GH = 'GH',
}

export enum Banks {
  GT = 'Gt bank',
  ZN = 'Zennith bank',
}


export enum MobileMoney {
  MTN = 'MTN',
}


export interface Coverage {
  country: Countries
  business: boolean
  personal: boolean
}


export interface Provider {
  name: string
  code: Codes
  icon: string
  website: string
  products: string[]
  coverage: Coverage[]
}

export interface Providers extends Array<Provider> {}

export interface ProvidersResponse extends Providers {}

export interface AuthenticateDetails {
  name: string 
  password: string
  email: string
}

export interface AccountDetails  extends Omit<Account, 'id' | 'consentExpirationTime'> {}

export interface FinancialInstitution {
  code: Codes
  authenticate: (details: AuthenticateDetails) => Promise<AccountDetails[]>
}
