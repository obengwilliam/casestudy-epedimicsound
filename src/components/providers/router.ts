import express from 'express'
import * as ctrl from './controller'

export default express.Router().get('/', ctrl.getAllProviders)
