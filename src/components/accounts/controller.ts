import {get, linkToMerchant} from './service'
import {verifyToken} from '../links/service'
import respond from '../../lib/respond'
import {Response, Request, NextFunction} from 'express'
import {LinkAccountDetails, Account, AccountsResponse} from './types'


const responseTransformers = {
  account (accountDetails: Account[]): AccountsResponse{
    return accountDetails.map(accountDetails=>({...accountDetails, consentExpirationTime: accountDetails.consentExpirationTime.toISOString()}))
  }
}

export const getAccountDetails = function (req: Request, res: Response, next: NextFunction): void {
  get(req.params.accountId)
    .then(respond({res}))
    .catch(next)
}


export const linkAccountsToMerchant =  function (req: Request, res: Response, next: NextFunction): void {

  verifyToken(req.body.token).then(({merchantId, accessLimitInMonths}): LinkAccountDetails=>({
    ...req.body, 
    merchantId, 
    limitInMonths: accessLimitInMonths,
  }))
    .then(linkToMerchant)
    .then(responseTransformers.account)
    .then(respond({res})).catch(next)


  // (async ()=>{
  //   try {
  //     const {merchantId, accessLimitInMonths} = await  verifyToken(req.body.token)
  //     const accountDetails: LinkAccountDetails = {...req.body, merchantId, limitInMonths: accessLimitInMonths}
  //     const accounts  = await linkToMerchant(accountDetails)
  //     const transformedAccounts = responseTransformers.account(accounts)
  //     return respond({res})(transformedAccounts)
  
  //   } catch (error) {
  //     next(error)
  //   }
  // })()
    
}
