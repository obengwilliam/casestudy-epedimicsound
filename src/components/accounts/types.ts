import {Countries, Codes} from '../providers/types'

export enum TypeOfAccount {
  LN = 'loan',
  BRK = 'brokerage',
  DEP = 'depository'
}
export enum SubTypeAccount {
  cur = 'current',
  sav = 'savings',
}

export enum Currency {
  nr  =  'NGN',
  ghc =  'GHS',
  doll  =  'USD',
  pnd =  'GBP',
  eur =  'EUR'
}
export interface  emailDetails  {
  email: string
  primary: boolean
}

export interface Account {
  id: string
  phoneNumber: string
  accountNumber: string
  customerName: string
  emails: emailDetails[]
  subtype: SubTypeAccount
  currency:  Currency
  type: TypeOfAccount
  branchCode: number
  openingBalance: number
  closingBalance: number
  consentExpirationTime: Date
  provider: Codes
  country: Countries
}


export interface LinkAccountDetails  {
  merchantId: string
  email: string
  name: string
  password: string
  limitInMonths: number
  providerCode: Codes
  country: Countries
}

export interface AccountResponse extends Omit<Account, 'consentExpirationTime'> {
  consentExpirationTime: string 
}

export interface AccountsResponse extends Array<AccountResponse> {}
