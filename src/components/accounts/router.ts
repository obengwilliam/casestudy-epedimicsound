import express from 'express'
import * as ctrl from './controller'
import status from '../../lib/status'
import mapErrorToStatus from '../../middlewares/error-map-status'
import {ErrorStatusMap} from '../../lib/errors'

const errorMap: ErrorStatusMap = {
  TokenVerificationError: status.UNAUTHORIZED
}
export default express.Router()
  .get('/:accountId', ctrl.getAccountDetails)
  .post('/auth', ctrl.linkAccountsToMerchant)
  .use(mapErrorToStatus(errorMap))
