import {Account, TypeOfAccount, Currency, LinkAccountDetails, SubTypeAccount} from './types'
import {Codes, Countries} from '../providers/types'
import moment from 'moment'
import fs from '../providers/fs'

export const get = async function (accountId: string): Promise<Account> {
  return {
    id: accountId,
    accountNumber: '393993',
    customerName: 'Obeng William',
    branchCode: 405,
    phoneNumber: '+233248567890',
    emails: [ {email: 'william@kudobuzz.com', primary:true} ],
    type: TypeOfAccount.DEP,
    provider: Codes.GT,
    country: Countries.GH,
    subtype: SubTypeAccount.cur,
    consentExpirationTime: new Date(),
    currency: Currency.ghc,
    openingBalance: 200.28,
    closingBalance: 899
  }
}


export const linkToMerchant = async function (accountDetails: LinkAccountDetails): Promise<Account []>{
  const institution = fs(accountDetails.providerCode, accountDetails.country)
  const consentExpirationTime = moment().add(accountDetails.limitInMonths, 'months').toDate()

  return await institution.authenticate(accountDetails).then(accounts=>{
    return accounts.map(account=>({
      ...account,
      id: '4555', 
      consentExpirationTime}))
  })

}
