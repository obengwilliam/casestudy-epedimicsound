export interface Statement {
  id: string
  account_id: string
  requested_at: string
  url: string
}

