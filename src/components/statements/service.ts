import {Statement} from './interface'

export const getAll = async (accountId: string): Promise<Statement[]> => {
  const today: string = Date().toString()
  const id: string = '3939394939'

  return [{
    id,
    account_id: accountId,
    requested_at: today,
    url: `https://statements.appruve.com/${id}`
  }]

}
