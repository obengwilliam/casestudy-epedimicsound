'use strict'
import requestLogger from 'express-pino-logger'
import express, {Response, Request, NextFunction, } from 'express'
import bodyParser from 'body-parser'
import toCamelCase from './middlewares/to-camelcase'
import providerRoutes from './components/providers/router'
import linkRoutes from './components/links/router'
import accountsRoutes from './components/accounts/router'
import * as openApiValidator from 'express-openapi-validator'
import config from './config'
import path from 'path'
import log from './lib/log'
import status from './lib/status'
import db from './lib/pg'

const spec = path.join(__dirname, 'api.yaml')

export default express()
  .use(requestLogger({logger: log}))
  .use('/spec', express.static(spec))
  .use(bodyParser.json())
  .use(bodyParser.text())
  .use(bodyParser.urlencoded({extended: false}))
  .use(
    openApiValidator.middleware({
      apiSpec: config('OPENAPI_PATH'),
      validateResponses: config('IS_DEV'),
      validateRequests: true,
    }),
  )
  .use(toCamelCase())
  .use('/v1', express.Router()
    .use('/links', linkRoutes)
    .use('/accounts', accountsRoutes)
    .use('/providers', providerRoutes)
  )
  .get('/healthz',  (_req, res, next)=>{
    db.raw('Select 1')
      .then(_=>{
        return res.status(status.OK).json({
          message: 'healthy'
        })
      })
      .catch(next)
  } )
  .use(function (_req, res, _next) {
    return res.status(status.OK).json({
      data: {
        message: 'Not Found'
      }
    })
  })
  .use((err: any, _req: Request, res: Response, _next: NextFunction) => {
    log.error(err)
    const status = err.status ?? 500
    const message = status === 500 ? 'Internal server error' : err.message

    return res.status(status).json({
      message,
      errors: err.errors,
    })
  })


