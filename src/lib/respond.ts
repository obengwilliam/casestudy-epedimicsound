import {OK } from 'http-status'
import snakecase from 'snakecase-keys' 

export default function respond ({res,  status=OK}){
  return (data: object): void => {
    res.status(status).json(snakecase({
      data: data
    }, {deep:true}))
  }
}
