import {CustomError} from 'ts-custom-error' 

type status = number 

export interface ErrorStatusMap {
  [key: string]: status
}

export class HttpException extends CustomError{
  status: number 
  constructor ( status: number,  message: string ){
    super(message)
    this.status = status 
  }
} 
