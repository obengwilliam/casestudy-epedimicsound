import pino from 'pino'

export default pino({
  name: 'financial-api'
})
