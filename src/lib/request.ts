
import got, {GotReturn} from 'got'
import {URL} from 'url'

export default function request (url: URL, defaulOptions): GotReturn{

  return got(url, {...{}, ...defaulOptions})
}
