import mail from '@sendgrid/mail'
import config from '../config'


mail.setApiKey(config('SENGRID_API_KEY'))


export default async function send (msg: {
  to: string
  from: string
  subject: string
  text?: string
  html: string
}): Promise<any> {
  return await mail
    .send(msg)

}

