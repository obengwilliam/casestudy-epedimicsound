
import {envSchema} from 'env-schema'
import path from 'path'
const OPEN_API_PATH = path.join(__dirname, '../api.yaml')


const schema = {
  type: 'object',
  required: [
    'PORT',
    'DB_HOST',
    'DB_PASSWORD',
    'DB_NAME',
    'TOKEN_PRIVATE_KEY',
    'SENGRID_API_KEY',
    'APPRUVE_FROM_EMAIL',
    'LINK_BASE_URL'
  ],
  properties: {
    DB_HOST: {
      type: 'string'
    },
    TOKEN_PRIVATE_KEY: {
      type: 'string'
    },
    LINK_BASE_URL: {
      type: 'string'
    },
    SENGRID_API_KEY: {
      type: 'string'
    },
    APPRUVE_FROM_EMAIL: {
      type: 'string'
    },
    OPENAPI_PATH: {
      type: 'string',
      default: OPEN_API_PATH
    },
    DB_PASSWORD: {
      type: 'string'
    },
    IS_DEV: {
      type: 'boolean',
      default: process.env.NODE_ENV === 'development'
    },
    DB_USER: {
      type: 'string'
    },
    DB_NAME: {
      type: 'string',
      default: 'appruve_financial_api'
    },
    PORT: {
      type: 'string',
      default: 3000
    }
  }
}

const config = envSchema({
  schema: schema,
  dotenv: true
})


const getConfig = (env: string): any => {
  if (config[env] === undefined) {
    throw new Error(`No config for env variable ${env}`)
  }
  return config[env]
}


export default getConfig
