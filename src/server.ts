import http from './app'
import log from './lib/log'
import './lib/pg'

const PORT = 4000

const onServerReady = (): void => {
  log.info(`listening on port ${PORT}`)
}

const server = http.listen(PORT, onServerReady)

let shuttingDown = false

const onShutDown =  (): void=>{
  log.info('sigterm , gracefully shuttingDown')
  if (shuttingDown) {
    return
  }
  shuttingDown = true

  server.close((err) => {
    if (err != null) {
      log.error('error during shutdown %s', err)
      process.exit(1)
    }
    log.info('Graceful shutdown finished.')
    process.exit(0)
  })
}

process.on('SIGTERM', onShutDown)
