# Financial Api
This api allows merchants to access a user's account data in any bank or mobile money account in Africa. This data provided includes the users account details, transactions, investments the person has made, any fraud he has made in the past, actionable insights, budgets , savings and financial goals.

Check out existing Api’s already in the market like Plaid , Tink, Nordigen, Okra , Mono


## Why are we building this ? 

Mostly, because there are several cases where access to the data above are needed by businesses but the process of connecting to a bank is such a big issue. Bank Apis are not readily available or provided in the right way, and sometimes you need data from varying banks. 
Also most existing api's as listed above do not focus on africa

Use Cases 
 - Receiving payments via direct bank is cheaper than credit cards but the authentication process for this does not seem to exist. Furthermore, checking the balance of these users in realtime does not seem to exist at all.
- Speed up on onboarding for your business  by easily getting access to statements.
- Reduce administration cost for some businesses by giving them an easy wayk to request access to financial account data from the customers.
- Help users have a good sense of teir finances across different banks.

# How should it work ? 
Merchant  A decides he needs access  to some financial data from a user.  
The user is sent a link that opens up some widget  or a custom page created by a merchant A or an email/sms containing a link to a widget. 
Once the user clicks on the link above above he/she will be asked to provide the username and password to a specific bank account.
The user accepts that he/she wants to give his data to mercmerchant A and accepts how far a merchants can retrieve data, specifically for things like transaction , statements which can go way back.
The user receives an email after providing consent to merchant A. 


# What the final build should look like ?
A bunch of apis that allows any merchants registered with us to access data from their customers in real time after a user has agreed to it. The Apis will include:
  - Account/Identity Api 
  - Transaction Api 
  - Statements Reports Api
  - Liabilities Api 
  - Savings and Budgets Api 
  - Accounts Aggregation Api 
  - Budget Api
  - Insights Api 


# Workflow 
##  Merchants Signup  Flow 
- A merchant signs up on the Dashboard which automatically gives him access to our financial Api.
- The merchant automatically is provided with the necessary client id and secret to allow him/her access to our api.
- The merchants must is encouraged to register webhooks for the following events
  New accounts events  -  When a user grants you access to some data
  Re Authentication events - When a user’s details no longer works when interacting with a provider.
  Updated Accounts events  - When a users details have received some updates 
  New Statements Generated - When a new statement is generated for a user.

## Merchant Integration Process
### Widget
- Using your client id and secret create a link token.
- This token allows you to form a  widget link ([api]/widgets/{token}) which returns a widget. This link will allow any user that receives it to authorize this api to give the merchants access to the user data.
- The metadata and the access token will be sent to your registered webhook endpoint.
- If the widget was integrated in your own domain, you shall receive the metadata(ACCOUNT ID) and access token in your success callback. 
- Using the access token, the merchants can  now make a request to any api as long as the user gives him/her access to it. 

### Accessing Financial Data 
 -  Having gone through the above process, a merchant can use his account id together with access token to make a request to any of the apis included:
  Account Api
  Transaction Api 
  Liabilities Api
  Statements Api 


## Users Flow 
- A users receives a widget link 
- Once the user clicks on the widget link, the user will get a pop up with actions to be taken. This includes:
  Giving consent to give a specific bank data to  the merchants whose name will show in the widgets.
- The user selects which bank and provides the necessary details according to the bank.
- The user agrees to the specific apis(accounts,transactions, statements) he/she wants to give to the merchants. 
- The user will now get an email and sms notice with an agreement covering the data to be shared and duration. An email will be sent to the bank also. 
- The email might contain a link for the user to revoke access immediately


# Architecture 
![image info](./docs/architecture.png)

# Tech Spec
[Api](./src/api.yaml)

# Tooling
  `husky`
  `typescript`
  `eslint`
  `standard`
  `ts-node`

# Usage
- `copy env.sample to .env`
- `docker-compose up -build`

# Npm Scripts

`build` - Compiles code to dist folder

`lint` - Lint all ts files

`start` - Start app by running src/index.ts and watching all ts files for changes

`lint:fix` - Fix all errors with issues
