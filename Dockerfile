FROM node:10-alpine

RUN apk update

WORKDIR /app

COPY package.json ./
COPY tsconfig.json ./
COPY . /app

RUN npm install


