import config from './src/config'

export default  {
  client: 'pg',
  connection: {
    host : config('DB_HOST'),
    user: config('DB_USER'),
    password: config('DB_PASSWORD'),
    database: config('DB_NAME')
  },
  debug: true,
  migrations: {
    tableName: 'migrations'
  }
}
